wartosc_kredytu = int(input("Podaj poczatkowa wartosc kredytu: "))
oprocentowanie = int(input("Podaj oprocentowanie kredytu: "))
kwota_raty = int(input("Podaj jaka ma byc kwota raty: "))

TEKST = "Twoja pozostała kwota kredytu to {}, to {} mniej niż w poprzednim miesiącu."

inflacja_styczen = 1.59282448436825
inflacja_luty = -0.453509101198007
inflacja_marzec = 2.32467171712441
inflacja_kwiecien = 1.261254407
inflacja_maj = 1.782526286
inflacja_czerwiec = 2.329384541
inflacja_lipiec = 1.502229842
inflacja_sierpien = 1.782526286
inflacja_wrzesien = 2.328848994
inflacja_pazdziernik = 0.616921348
inflacja_listopad = 2.352295886
inflacja_grudzien = 0.337779545
inflacja_styczen_2 = 1.577035247
inflacja_luty_2 = -0.292781443
inflacja_marzec_2 = 2.48619659
inflacja_kwiecien_2 = 0.267110318
inflacja_maj_2 = 1.417952672
inflacja_czerwiec_2 = 1.054243267
inflacja_lipiec_2 = 1.480520104
inflacja_sierpien_2 = 1.577035247
inflacja_wrzesien_2 = -0.07742069
inflacja_pazdziernik_2 = 1.165733399
inflacja_listopad_2 = -0.404186718
inflacja_grudzien_2 = 1.499708521

kwota_pozostala_styczen = (1+((inflacja_styczen+oprocentowanie)/1200))*wartosc_kredytu-kwota_raty
kwota_pozostala_luty = (1+((inflacja_luty+oprocentowanie)/1200))*kwota_pozostala_styczen-kwota_raty
kwota_pozostala_marzec = (1+((inflacja_marzec+oprocentowanie)/1200))*kwota_pozostala_luty-kwota_raty
kwota_pozostala_kwiecien = (1+((inflacja_kwiecien+oprocentowanie)/1200))*kwota_pozostala_marzec-kwota_raty
kwota_pozostala_maj = (1+((inflacja_maj+oprocentowanie)/1200))*kwota_pozostala_kwiecien-kwota_raty
kwota_pozostala_czerwiec = (1+((inflacja_czerwiec+oprocentowanie)/1200))*kwota_pozostala_maj-kwota_raty
kwota_pozostala_lipiec = (1+((inflacja_lipiec+oprocentowanie)/1200))*kwota_pozostala_czerwiec-kwota_raty
kwota_pozostala_sierpien = (1+((inflacja_sierpien+oprocentowanie)/1200))*kwota_pozostala_lipiec-kwota_raty
kwota_pozostala_wrzesien = (1+((inflacja_wrzesien+oprocentowanie)/1200))*kwota_pozostala_sierpien-kwota_raty
kwota_pozostala_pazdziernik = (1+((inflacja_pazdziernik+oprocentowanie)/1200))*kwota_pozostala_wrzesien-kwota_raty
kwota_pozostala_listopad = (1+((inflacja_listopad+oprocentowanie)/1200))*kwota_pozostala_pazdziernik-kwota_raty
kwota_pozostala_grudzien = (1+((inflacja_grudzien+oprocentowanie)/1200))*kwota_pozostala_listopad-kwota_raty
kwota_pozostala_styczen_2 = (1+((inflacja_styczen_2+oprocentowanie)/1200))*kwota_pozostala_grudzien-kwota_raty
kwota_pozostala_luty_2 = (1+((inflacja_luty_2+oprocentowanie)/1200))*kwota_pozostala_styczen_2-kwota_raty
kwota_pozostala_marzec_2 = (1+((inflacja_marzec_2+oprocentowanie)/1200))*kwota_pozostala_luty_2-kwota_raty
kwota_pozostala_kwiecien_2 = (1+((inflacja_kwiecien_2+oprocentowanie)/1200))*kwota_pozostala_marzec_2-kwota_raty
kwota_pozostala_maj_2 = (1+((inflacja_maj_2+oprocentowanie)/1200))*kwota_pozostala_kwiecien_2-kwota_raty
kwota_pozostala_czerwiec_2 = (1+((inflacja_czerwiec_2+oprocentowanie)/1200))*kwota_pozostala_maj_2-kwota_raty
kwota_pozostala_lipiec_2 = (1+((inflacja_lipiec_2+oprocentowanie)/1200))*kwota_pozostala_czerwiec_2-kwota_raty
kwota_pozostala_sierpien_2 = (1+((inflacja_sierpien_2+oprocentowanie)/1200))*kwota_pozostala_lipiec_2-kwota_raty
kwota_pozostala_wrzesien_2 = (1+((inflacja_wrzesien_2+oprocentowanie)/1200))*kwota_pozostala_sierpien_2-kwota_raty
kwota_pozostala_pazdziernik_2 = (1+((inflacja_pazdziernik_2+oprocentowanie)/1200))*kwota_pozostala_wrzesien_2-kwota_raty
kwota_pozostala_listopad_2 = (1+((inflacja_listopad_2+oprocentowanie)/1200))*kwota_pozostala_pazdziernik_2-kwota_raty
kwota_pozostala_grudzien_2 = (1+((inflacja_grudzien_2+oprocentowanie)/1200))*kwota_pozostala_listopad_2-kwota_raty

roznica_styczen = wartosc_kredytu - kwota_pozostala_styczen
roznica_luty = kwota_pozostala_styczen - kwota_pozostala_luty
roznica_marzec = kwota_pozostala_luty - kwota_pozostala_marzec
roznica_kwiecien = kwota_pozostala_marzec - kwota_pozostala_kwiecien
roznica_maj = kwota_pozostala_kwiecien - kwota_pozostala_maj
roznica_czerwiec = kwota_pozostala_maj - kwota_pozostala_czerwiec
roznica_lipiec = kwota_pozostala_czerwiec - kwota_pozostala_lipiec
roznica_sierpien = kwota_pozostala_lipiec - kwota_pozostala_sierpien
roznica_wrzesien = kwota_pozostala_sierpien - kwota_pozostala_wrzesien
roznica_pazdziernik = kwota_pozostala_wrzesien - kwota_pozostala_pazdziernik
roznica_listopad = kwota_pozostala_pazdziernik - kwota_pozostala_listopad
roznica_grudzien = kwota_pozostala_listopad - kwota_pozostala_grudzien
roznica_styczen_2 = kwota_pozostala_grudzien - kwota_pozostala_styczen_2
roznica_luty_2 = kwota_pozostala_styczen_2 - kwota_pozostala_luty_2
roznica_marzec_2 = kwota_pozostala_luty_2 - kwota_pozostala_marzec_2
roznica_kwiecien_2 = kwota_pozostala_marzec_2 - kwota_pozostala_kwiecien_2
roznica_maj_2 = kwota_pozostala_kwiecien_2 - kwota_pozostala_maj_2
roznica_czerwiec_2 = kwota_pozostala_maj_2 - kwota_pozostala_czerwiec_2
roznica_lipiec_2 = kwota_pozostala_czerwiec_2 - kwota_pozostala_lipiec_2
roznica_sierpien_2 = kwota_pozostala_lipiec_2 - kwota_pozostala_sierpien_2
roznica_wrzesien_2 = kwota_pozostala_sierpien_2 - kwota_pozostala_wrzesien_2
roznica_pazdziernik_2 = kwota_pozostala_wrzesien_2 - kwota_pozostala_pazdziernik_2
roznica_listopad_2 = kwota_pozostala_pazdziernik_2 - kwota_pozostala_listopad_2
roznica_grudzien_2 = kwota_pozostala_listopad_2 - kwota_pozostala_grudzien_2

print(TEKST.format(kwota_pozostala_styczen, roznica_styczen))
print(TEKST.format(kwota_pozostala_luty, roznica_luty))
print(TEKST.format(kwota_pozostala_marzec, roznica_marzec))
print(TEKST.format(kwota_pozostala_kwiecien, roznica_kwiecien))
print(TEKST.format(kwota_pozostala_maj, roznica_maj))
print(TEKST.format(kwota_pozostala_czerwiec, roznica_czerwiec))
print(TEKST.format(kwota_pozostala_lipiec, roznica_lipiec))
print(TEKST.format(kwota_pozostala_sierpien, roznica_sierpien))
print(TEKST.format(kwota_pozostala_wrzesien, roznica_wrzesien))
print(TEKST.format(kwota_pozostala_pazdziernik, roznica_pazdziernik))
print(TEKST.format(kwota_pozostala_listopad, roznica_listopad))
print(TEKST.format(kwota_pozostala_grudzien, roznica_grudzien))
print(TEKST.format(kwota_pozostala_styczen_2, roznica_styczen_2))
print(TEKST.format(kwota_pozostala_luty_2, roznica_luty_2))
print(TEKST.format(kwota_pozostala_marzec_2, roznica_marzec_2))
print(TEKST.format(kwota_pozostala_kwiecien_2, roznica_kwiecien_2))
print(TEKST.format(kwota_pozostala_maj_2, roznica_maj_2))
print(TEKST.format(kwota_pozostala_czerwiec_2, roznica_czerwiec_2))
print(TEKST.format(kwota_pozostala_lipiec_2, roznica_lipiec_2))
print(TEKST.format(kwota_pozostala_sierpien_2, roznica_sierpien_2))
print(TEKST.format(kwota_pozostala_wrzesien_2, roznica_wrzesien_2))
print(TEKST.format(kwota_pozostala_pazdziernik_2, roznica_pazdziernik_2))
print(TEKST.format(kwota_pozostala_listopad_2, roznica_listopad_2))
print(TEKST.format(kwota_pozostala_grudzien_2, roznica_grudzien_2))








